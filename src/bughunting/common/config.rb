#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "yaml"

class BaseConfig
  def initialize(options = nil)
    @options = self.class.get_defaults
    merge! options unless options.nil?
  end

  def self.get_defaults
    {}.clone
  end

  def self.load_file(filename)
    options = YAML.load_file filename
    self.new options
  end

  def [](path)
    index_get @options, path
  end

  def []=(path, value)
    index_set @options, path, value
  end

  def merge!(other_options)
    merge_recursive @options, other_options
  end

  private

  def index_get(hash, path)
    if path.nil?
      return hash
    elsif not hash.kind_of? Hash
      return nil
    end

    (option, next_path) = path.split(".", 2)
    if hash.include? option
      return index_get hash[option], next_path
    else
      return nil
    end
  end

  def index_set(hash, path, value)
    (option, next_path) = path.split(".", 2)

    if not hash.include? option
      if next_path.nil?
        hash[option] = value
      else
        hash[option] = {}
        index_set hash[option], next_path, value
      end
    elsif next_path.nil?
      hash[option] = value
    elsif hash[option].kind_of? Hash
      index_set hash[option], next_path, value
    else
      raise Exception.new "Parent node is not a Hash."
    end
  end

  def merge_recursive(first, second)
    second.each do |key, value|
      if first.include? key and first[key].kind_of? Hash and value.kind_of? Hash
        merge_recursive(first[key], value)
      else
        first[key] = value
      end
    end
  end
end
