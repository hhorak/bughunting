#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/check/task"
require "bughunting/common"
require "bughunting/server/config"
require "fileutils"
require "shellwords"
require "xmlrpc/client"
require 'pathname'

class DevelopmentController
  def initialize(task_dir, server_config, verbose = false)
    task_dir = File.expand_path task_dir

    @server_config = ServerConfig.load_file server_config
    @task = Task.new task_dir
    @verbose = !!verbose

    @xmlrpc = XMLRPC::Client.new3({ "host" => "::1", "port" => @server_config["listen.port"] })
  end

  def check
    setup(noclean: true)

    begin
      puts "Requesting evaluation"
      result = @xmlrpc.call("bughunting.local_check", @task.dir, @task.work_dir, @verbose)
    rescue Errno::ECONNREFUSED
      puts "Cannot connect to the testing server."
      return false
    rescue XMLRPC::FaultException => e
      puts "Server error: #{e.message}"
      return false
    end

    (success, message) = result

    puts "Result: %s, %s" % [success ? "success".green.bold : "failure".red.bold, message]
    return success
  end

  def setup(noclean: false)
    if File.exists? @task.cache_dir
      unless noclean
        puts "Source codes are already set up."
        clean
      end
    else
      system "cp", "-ar", @task.source_dir, @task.work_dir
      run_setup_commands
      create_cache
    end
  end

  def clean
    if test("d", @task.cache_dir)
      system "mkdir", "-p", @task.work_dir unless test("d", @task.work_dir)
      system "rsync", "-a", "--del", @task.cache_dir + "/", @task.work_dir
    end
  end

  def superclean
    system "rm", "-rf", @task.work_dir
    unless @task.symlinked_source
      system "rm", "-rf", @task.cache_dir
    end
  end

  def patch
    Dir.chdir @task.dir
    system "diff", "-Naur",
      relative_path(@task.cache_dir), relative_path(@task.work_dir)
  end

  private

  def relative_path path
    Pathname.new(path).relative_path_from(Pathname.new(@task.dir)).to_s
  end

  def run_setup_commands
    commands = [ "cd '#{@task.work_dir}'" ]
    commands += @task.config["setup_commands"]
    system "sh", "-c", commands.join(";")
  end

  def create_cache
    system "cp", "-ar", @task.work_dir, @task.cache_dir
  end

  def system(*command)
    $stderr.puts "+ " + Shellwords.join(command)
    Kernel.system *command
    $?
  end
end
