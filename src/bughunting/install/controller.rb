#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require 'thwait'

require "bughunting/check/task"
require "bughunting/server/config"
require "bughunting/web/config"
require "bughunting/install/installer"

class InstallController
  def initialize(server_config_name, web_config_name, debug=false)
    @config = ServerConfig.load_file server_config_name
    @web_config = WebConfig.load_file web_config_name
    @tasks = Task.load_dir @config["paths.tasks"]
    @debug = debug
  end

  def install(host)
    installer = Installer.new(host,
                              @config["clients.user"],
                              @config["clients.identity_file"],
                              @config["clients.sources_dir"],
                              @config["clients.cache_dir"],
                              @debug
                             )

    installer.try_ssh
    installer.clean_full unless installer.error
    installer.push_sources @tasks unless installer.error
    installer.run_install @tasks unless installer.error
    installer.run_setup @tasks unless installer.error
    installer.create_cache @tasks unless installer.error
    # TODO: propagate exit status from sub-commands
    if installer.error then 1 else 0 end
  end

  def install_all
    threads = []
    @web_config["players"].keys.each do |ip|
      unless ip == "127.0.0.1"
        threads <<  Thread.new do
          install ip
        end
      end
    end
    ThreadsWait.all_waits *threads
    threads.each do |thread|
      if thread.value != 0
        STDERR.puts "ERROR: thread failed: #{thread.value}"
        return thread.value
      end
    end
    return 0
  end
end
