#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/common"

# local check
require "bughunting/check/exec"
require "bughunting/check/jail"
require "bughunting/check/limits"
require "bughunting/check/task"

# sending results to web interface
require "net/http"
require "uri"

require "time"

class BaseService
  private

  def get_ip(request)
    request.peeraddr[3]
  end

  def require_localhost!(request)
    unless ["127.0.0.1", "::1"].include? get_ip(request)
      raise WEBrick::HTTPStatus::Forbidden
    end
  end

  def get_limits(server_config, task_config)
    time = task_config["limits.cputime_seconds"] || server_config["limits.cputime_seconds"]
    memory = task_config["limits.memory_megabytes"] || server_config["limits.memory_megabytes"]
    processes = task_config["limits.process_count"] || server_config["limits.process_count"]

    Limits.new time, memory, processes
  end
end

# TODO: rename
class DevelServerService < BaseService
  def initialize(server_config, user, carrier_factory)
    @server_config = server_config
    @user = user
    @carrier_factory = carrier_factory
  end

  def local_check(request, task_dir, work_dir, verbose)
    require_localhost! request

    task = Task.new task_dir

    carrier = @carrier_factory.build_local work_dir

    jail = Jail.new @user
    jail.create task, carrier

    limits = get_limits @server_config, task.config

    puts "=== Starting local test ===".yellow.bold
    puts "task: %s" % task_dir
    puts "work: %s" % work_dir
    puts "check: %s" % jail.path.bold

    exec = Exec.new @user, jail.exec_dir, limits, jail.log, jail.check_logfile
    exit_status = exec.run task.config["check_script"]

    if exit_status.kind_of? Integer
      score = task.score exit_status
      result = "score %d (exit status %d)" % [score, exit_status]
      result += "\n#{jail.check_logfile}:\n" + File.open(jail.check_logfile, "r").read if verbose
    else
      score = 0
      result = exit_status.to_s
    end

    # TODO
    jail.set_world_readable
    #jail.destroy

    return [score > 0, result]
  end
end

# TODO: rename
class CompetitionServerService < BaseService
  def initialize(server_config, users, tasks, carrier_factory)
    @server_config = server_config
    @users = users
    @tasks = tasks
    @carrier_factory = carrier_factory

    @web_submit_uri = URI::HTTP.build({
      :host => @server_config["web.ip"],
      :port => @server_config["web.port"],
      :path => "/_service/solved"
    })
  end

  def list_tasks(request)
    tasks_info = []
    @tasks.each do |name, task|
      tasks_info << { "name" => name,  "summary" => task.config["summary"] }
    end
    tasks_info
  end

  def check(request, task_name)
    begin
      _check(request, task_name)
    rescue Exception => e
      puts e
      puts e.backtrace
    end
  end

  def _check(request, task_name)
    ip = get_ip request

    task = @tasks[task_name]
    if task.nil?
      return [false, "This task does not exist."]
    end

    user = @users.get_free ip
    if user.nil?
      return [false, "Too many tests running on the server. Please, try again later."]
    end

    begin
      carrier = @carrier_factory.build_remote \
        @server_config["clients.user"],
        ip,
        @server_config["clients.identity_file"],
        @server_config["clients.sources_dir"], task_name

      jail = Jail.new user
      begin
        jail.create task, carrier
      rescue => e
        return [false, e.message]
      end
      limits = get_limits @server_config, task.config

      exec = Exec.new user, jail.exec_dir, limits, jail.log, jail.check_logfile
      exit_status = exec.run task.config["check_script"]

      if exit_status.kind_of? Integer
        score = task.score exit_status
        if score > 0
          begin
            bz_score = web_send_result ip, task_name, score
            if score == bz_score
              result = "You gain %d score points." % score
            elsif score < bz_score
              result = "You gain %d score points. But you already submitted a better solution (%d)." % [score, bz_score]
            else
              result = "You gain %d score points. It would be %d without hints." % [bz_score, score]
            end
          rescue => e
            result = "Call for help. Your fix is correct but something went wrong (%s)" % e.message
            puts e.message, e.backtrace
            score = 0
          end
        else
          result = "No score points for this. Try again."
        end
      else
        score = 0
        if exit_status == :timeout
          result = "Timeout expired. Your fix introduced an infinite loop or a significant slow-down."
        else
          result = "Call for help, something went wrong (%s)." % exit_status
        end
      end

      return [score > 0, result]
    ensure
      @users.return ip
      unless jail.nil?
        archive_dir = File.join \
          @server_config["paths.archive"],
          "%s_%s_%s" % [ip, Time.now::to_i, File.basename(jail.path)]
        jail.archive archive_dir
      end
    end
  end

  private

  # TODO: move this out of this class
  def web_send_result(player, task, score)
    result = Net::HTTP::post_form(@web_submit_uri, {
      :player => player,
      :task => task,
      :score => score,
    })
    result.body.to_i
  end
end
