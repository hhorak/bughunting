#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "timeout"

class Exec
  def initialize(user, exec_dir, limits, log, check_logfile)
    @user = user
    @exec_dir = exec_dir
    @limits = limits
    @log = log
    @check_logfile = check_logfile
  end

  def run(command)
    result = run_restricted command
    @log.msg "test result: %s" % result
    result
  end

  def interrupt
    # FIXME: not tested, thread safety?
    kill_remaining
  end

  private

  STATUS_EXEC_ERROR = 250

  def build_command(command)
    ulimit_cmd = @limits.ulimit_command

    cmds = []
    cmds << 'cd "%s"' % @exec_dir
    cmds << ulimit_cmd unless ulimit_cmd.nil?
    cmds << "./#{command}"

    # FIXME: Daniel Walsh wrote about sudo vs runuser...
    ["/usr/bin/sudo", "-u", @user, "/bin/sh", "-x", "-c", "{ %s; } >> #{@check_logfile} 2>&1" % cmds.join("; ")]
  end

  def kill_remaining
    @log.msg "killing remaining processes"
    system *["/usr/bin/pkill", "-KILL", "-u", @user]
    # 0 - at least one process matched, 1 - no process matched
    [0, 1].include? $?
  end

  def run_restricted(script)
    command = build_command script
    exit_status = nil
    begin
      @log.cmd command
      @log.msg "timeout: %d seconds" % @limits.time
      Timeout.timeout(@limits.time) do
        exit_status = run_forked command
      end
    rescue Timeout::Error
      @log.msg "execution timeout"
      exit_status = :timeout
    ensure
      kill_remaining
    end

    exit_status
  end

  def run_forked(command)
    cmd_pid = Process.fork do
      begin
        #@log.cmd command
        exec *command
      rescue Exception => exception
        @log.msg exception.message, exception.backtrace
        exit STATUS_EXEC_ERROR
      end
    end

    exit_info = Process.waitpid2(cmd_pid)[1]
    exit_status = exit_info.exitstatus

    @log.msg "finished, exit status %d" % exit_status
    if exit_status == STATUS_EXEC_ERROR
      @log.msg "exec() error"
      exit_status = :error
    end

    exit_status
  end

  def system(*params)
    @log.cmd params
    Kernel.system *params
  end
end
