#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++

require "bughunting/check/task_config"
require "yaml"

class Task
  attr_reader :dir
  attr_reader :config_file, :source_dir, :check_dir
  attr_reader :work_dir, :cache_dir
  attr_reader :config
  attr_reader :symlinked_source

  def initialize(dir)
    @dir = File.expand_path dir
    @config_file = File.join @dir, "task.yml"
    @config = TaskConfig.load_file @config_file
    init_paths
  end

  def score(exit_status)
    # 5: true    -> max points
    # 5: 7      -> 7 points
    # 0..5: true  -> max points
    # 0..5: 2    -> (exit_status + 2) points

    score = 0

    if @config["check_exit_status"].empty?
      @config["check_exit_status"][0] = true
    end

    @config["check_exit_status"].each do |cond, rule|
      if cond.kind_of? Integer and cond == exit_status
        score = rule
        break
      elsif cond.kind_of? String and cond =~ /^\d+..\d+$/ and eval(cond).include?(exit_status)
        if rule.kind_of? Integer
          score = exit_status + rule
        else
          score = rule
        end
        break
      end
    end

    # range validation
    if score == true or score > @config["max_score"]
      score = @config["max_score"]
    elsif score < 0
      score = 0
    end

    score
  end

  def self.load_dir(dirname)
    tasks = {}
    dir = Dir.new dirname
    dir.each do |entry|
      next if entry.start_with? "."

      task_dir = File.join dirname, entry
      next unless File.directory? task_dir

      begin
        task = Task.new task_dir
        tasks[entry] = task
      rescue
      end
    end
    tasks
  end

  private

  def init_paths
    @work_dir = File.join @dir, "work.tmp"
    @cache_dir = File.join @dir, "cache.tmp"
    @check_dir = File.join @dir, "check"
    @source_dir = File.join @dir, "source"

    @symlinked_source = File.symlink? @source_dir
    if @symlinked_source
      from = File.dirname @source_dir
      target = File.readlink @source_dir
      sym_source = File.expand_path target, from
      sym_task_dir = File.dirname sym_source

      # inherit source dir, cache dir, and setup_commands from symlinked task
      sym_task = Task.new sym_task_dir
      @source_dir = sym_task.source_dir
      @cache_dir = sym_task.cache_dir
      @config["setup_commands"] = sym_task.config["setup_commands"]
    end
  end
end
