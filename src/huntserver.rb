#!/usr/bin/ruby


require "bughunting/common"
require "bughunting/server/controller"
require "rubygems"
require "optparse"

config_filename = "/etc/bughunting/server.yml"
local_mode = false

parser = OptionParser.new do |opts|
  opts.banner = "usage: huntserver options"
  opts.on("-l", "--local", "Start the server in local development mode.") do local_mode = true end
  opts.on("-c:", "--config", "Set server configuration file") do |value| config_filename = value end
end.parse!

if not ARGV.empty?
  puts "error"
  exit 1
end

controller = ServerController.new config_filename, local_mode
controller.run
