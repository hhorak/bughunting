#!/usr/bin/ruby
#--
# Copyright (c) 2011-2016 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#++


require "rubygems"

require "bughunting/web/controller"
require "erb"
require "json"
require "sinatra"
require "haml"

include ERB::Util

controller = WebController.new "/etc/bughunting/web.yml"

# server configuration

configure do
  set :bind, controller.config["listen.ip"]
  set :port, controller.config["listen.port"]
  set :environment, :production
  set :logging, false

  set :controller, controller
end

# helper functions

helpers do
  def local_access?
    ["127.0.0.1", "::1"].include? request.ip
  end

  def service_access?
    request.ip == settings.controller.config["check_server.ip"]
  end

  def condition_enforce_uri(condition, target_uri, return_uri = "/")
    if condition and request.path != target_uri
      redirect to(target_uri)
    elsif not condition and request.path == target_uri
      redirect to(return_uri)
    else
      # true if current URI should be enforced
      condition
    end
  end

  def machine_name(ip, verbose = false)
    name = settings.controller.machine_name ip
    if verbose and name != ip
      "%s (%s)" % [name, ip]
    else
      name
    end
  end
end

# access filtering

before do
  @player = request.ip.force_encoding("UTF-8")
  @player_name = controller.players.name @player
  @machine_name = machine_name @player
end

before do
  next if service_access? or local_access?

  next if condition_enforce_uri @player_name.nil?, "/registration/"
  next if condition_enforce_uri !controller.game_running?, "/welcome/"
end

# pre-game

get "/registration/" do
  haml :registration, :locals => {
    :machine_name => @machine_name
  }
end

post "/registration/" do
  controller.players.register @player, params[:name], params[:email]
  redirect("/")
end

get "/welcome/" do
  haml :welcome
end

# client requests

get "/" do
  haml :index, :locals => {
    :tasks => controller.tasks_list(@player),
    :player_name => @player_name,
    :machine_name => @machine_name,
    :total_score => controller.scores.total(@player),
    :local_access => local_access?,
  }
end

get "/task/:name/" do
  task_name = params[:name]

  task = controller.tasks[task_name]
  halt 404 if task.nil?

  haml :task, :locals => {
    :task => task,
    :score => controller.scores.task(@player, task_name),
    :revealed_hints => controller.hints.number_of_revealed(@player, task_name),
  }
end

get "/task/:name/hint" do
  task_name = params[:name]
  hint_number = params[:num].to_i

  # task existence
  task = controller.tasks[task_name]
  halt 404 if task.nil?

  # hint existence
  halt 404 if hint_number < 1 or hint_number > task.config["hints"].count

  revealed = controller.hints.number_of_revealed @player, task_name
  if revealed + 1 == hint_number
    controller.hints.reveal @player, task_name, hint_number
  end

  redirect to("/task/%s/" % task_name)
end

# results

get "/scoreboard/" do
  halt 403 unless local_access?

  if params.include? "json"
    ranking = controller.scores.ranking
    ranking.collect! do |rank|
      {
        :player => rank[:player],
        :name => machine_name(rank[:player]),
        :score => rank[:score],
      }
    end
    best_rank = ranking.max_by do |rank| rank[:score] end

    JSON.generate({
      "max_score" => best_rank.nil? ? 0 : best_rank[:score],
      "ranking" => ranking,
    })
  else
    haml :scoreboard, :layout => false
  end
end

get "/results/" do
  halt 403 unless local_access?
  haml :results, :layout => false, :locals => {
    :ranking => controller.scores.ranking
  }
end

#admin

get "/admin/" do
        halt 403 unless local_access?

        haml :admin, :layout => true, :locals => {
                 :running => controller.game_running?
        }
end

post "/admin/start" do
        halt 403 unless local_access?

        controller.start_game
        redirect("/admin/")
end

post "/admin/stop" do
        halt 403 unless local_access?

        controller.stop_game
        redirect("/admin/")
end

post "/admin/archive" do
        halt 403 unless local_access?

        controller.archive_game
        redirect("/admin/")
end

# service requests

post "/_service/solved" do
  halt 403 unless service_access?

  # BEWARE: @player != player

  player = params[:player]
  task_name = params[:task]
  score = params[:score].to_i

  task = controller.tasks[task_name]
  halt 404 if task.nil?

  current_score = controller.scores.task player, task_name
  revealed_hints = controller.hints.number_of_revealed player, task_name
  possible_max = task.config["max_score"] - revealed_hints

  if score > possible_max
    score = possible_max
  end

  if score > current_score
    controller.scores.set player, task_name, score
    [200, score.to_s]
  else
    [200, current_score.to_s]
  end
end

# errors

not_found do
  haml :e404
end
