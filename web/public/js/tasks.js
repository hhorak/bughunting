$(document).ready(function() {
  // clickable lines in task list
  $("#task-list tr").click(function() {
    var target = $(this).find("a").attr("href");
    if (target)
      window.location = target;
  });
  // confirm hint reveal
  $("#reveal-next-hint").click(function() {
    return confirm("Do you want to reveal this hint and loose one score point?");
  });
});
