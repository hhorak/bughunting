#!/usr/bin/ruby

require "test/unit"
require "bughunting/check/limits"

class TestCheckLimits < Test::Unit::TestCase
  def test_any
    limit = Limits.new
    assert_nil limit.time
    assert_nil limit.memory
    assert_nil limit.process_count
    assert_nil limit.ulimit_command
  end

  def test_time
    limit = Limits.new 10
    assert_equal 10, limit.time
    assert_equal "ulimit -t 10", limit.ulimit_command
    limit.time = 20
    assert_equal "ulimit -t 20", limit.ulimit_command
  end

  def test_memory
    limit = Limits.new nil, 128000
    assert_equal 128000, limit.memory
    assert_equal "ulimit -m 128000", limit.ulimit_command
    limit.memory = nil
    assert_nil limit.ulimit_command
  end

  def test_process_count
    limit = Limits.new nil, nil, 32
    assert_equal 32, limit.process_count
    assert_equal "ulimit -u 32", limit.ulimit_command
    limit.process_count = 1
    assert_equal 1, limit.process_count
  end

  def test_combined
    limit = Limits.new 1, 2, 3
    assert_equal 1, limit.time
    assert_equal 2, limit.memory
    assert_equal 3, limit.process_count
    assert_equal "ulimit -t 1 -m 2 -u 3", limit.ulimit_command
    limit.process_count = 10
    limit.memory = nil
    limit.time = 4
    assert_equal "ulimit -t 4 -u 10", limit.ulimit_command
  end
end
